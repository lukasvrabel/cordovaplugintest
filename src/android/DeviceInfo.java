package com.ginasystem.plugins;
 
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;

import android.app.Activity;
import android.content.Intent;

// for IMEI
import android.content.Context;
import android.telephony.TelephonyManager;

public class DeviceInfo extends CordovaPlugin {
    public static final String ACTION_GET_IMEI = "getIMEI";
    
    
    
    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        try {
            if (ACTION_GET_IMEI.equals(action)) {
            	TelephonyManager telephonyManager = (TelephonyManager) this.cordova.getActivity().getSystemService(Context.TELEPHONY_SERVICE);
            	String IMEI = telephonyManager.getDeviceId();
                callbackContext.success(IMEI);               
            }
            
            callbackContext.error("Invalid action");
            return false;
        } catch(Exception e) {
            System.err.println("Exception: " + e.getMessage());
            callbackContext.error(e.getMessage());
            return false;
        } 
    }
}